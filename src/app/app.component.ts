import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  //testClass= 'w3-pale-blue w3-card-4'
  usableArray =[]
  
  usableJson = {}
  
  usableArrayRecieved(val){
     console.log(val) 
     this.usableArray = val
    }
  
  usableJsonRecieved(val){
      console.log(val)
      this.usableJson = val
      }
  
  
  doSomeThing(x,z,y){
      console.log("do", y)
      }
  // key, value, keyEditable, valueEditable, element, inputType, min , max , step , deletable, options, 
  // valuePlaceholder keyPlaceholder 
  
  // metadata : {title:"testEditor", keysEditable: true, valuesEditable, propsDeletable:true ,extendable:true, save: doSomeThing(), cancel:doSomeThing }    
  testObject = [
     {  key:"key1", keyEditable: false, value:"value1", valueEditable:false,  valueClass:'',element:"input", inputType:"color", deletable: false  },
    // {  key:"key Editable", keyEditable: true , value:"value2", valueEditable: false , element:"input", inputType:"text", deletable: false  },
    // {  key:"value Editable deletable", keyEditable: false, value:"value3", valueEditable: true  , element:"input", inputType:"text", deletable: true  },
    // {  key:"number editable", keyEditable: false, value:"4"    , valueEditable: true , element:"input", inputType:"number", deletable: false  },
    // {  key:" deletable ", keyEditable: false, value:"value5", valueEditable: false , element:"input", inputType:"text", deletable: true  },
    // {  key:"textarea deletable ", keyEditable: false, value:"this is Textarea",  element:"textarea"  },
     
     /*{  key:"select from list", keyEditable: false, 
            element:"select", valuePlaceholder:"choose", value:"selected option Key", valueEditable: false, 
            options:[ {key:"key1", value:"1"},{key:"key2", value:"2"},  { key:"selected option Key", value:"selected option value"} ,{key:"key3", value:"3"},{key:"key4", value:"4"} ] , 
            deletable: true  },
     
     {  key:"check from list", keyEditable: false, 
            element:"check", valueEditable: false,
            value  :  [ {key:"key1", value:true},{key:"key2", value:false},  { key:"key3", value:false} ,{key:"key4", value:true},{key:"key5", value:false} ] ,
            //options:[ {key:"key1", value:true},{key:"key2", value:false},  { key:"key3", value:false} ,{key:"key4", value:true},{key:"key5", value:false} ] , 
            deletable: false  },
     
     {  key:"check only one ", keyEditable: false, 
            element:"radio", valueEditable: false,
            value  :  "selected option Key" ,
            options:[ {key:"key1", value:"1"}, {key:"key2", value:"2"},  { key:"selected option Key", value:"selected option value"} ,{key:"key3", value:"3"},{key:"key4", value:"4"} ] , 
            deletable: false  },
     */
    // {  key:"minimal ", value:"value6"  },
  ]
  objMetaData = {title:"testEditor", 
                // titleClass :'w3-amber w3-panel',
                 keysEditable: true, 
                 extendable:true, save: this.doSomeThing, saveTxt:"حفظ", saveParameter:"[1,2,3]", }    
  
   
    
  title = 'app';
}
