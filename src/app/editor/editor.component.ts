import { Component, OnInit, Input , Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'obj-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {
  @ Input () object ;
  @ Input () metadata ;
  
  usableArray =[]
  usableJson = {}
  
  @ Output() getUsableArray = new EventEmitter();
  @ Output() getUsableJson = new EventEmitter();

  /* metadatexample
   * = {title:"", keysEditable: true, valuesEditable: true, propsDeletable:true, extenable:true, extendableTxt:""}
   * PropsClasses , keysClasses valuesParentsClasses valuesClasses
   * 
   * object= [
   *         { key:"", keyEditable:"",    class:"", keyClass:""
   *           value:"", valueEditable: true, valueParentClass:"", valueClass:""
   *           element:"input", inputType:"", min:"", max:"", step:""
   *           deletable : true
   *          }
   * ] 
   * 
   */ 
  constructor() { }
  
      
  hasProp(obj, key) {
      return obj.hasOwnProperty(key);
    }  
  
  objectedited(){
    let array = []      
    for (let prop of this.object){
        let newProp = { key:prop.key , value: prop.value }
        array.push(newProp)
    }
    this.getUsableArray.emit(array);
    this.usableArray = array
    
    let json = {}
    for (let prop of this.object){
       if (prop.element == 'check'){
           let val = {};
           for ( let opt of prop.value ){
               val[opt.key] = opt.value
               }
           json[prop.key] = val
       }else{
            console.log(prop) 
            json[prop.key] =    prop.value 
       }
    }    
    this.getUsableJson.emit(json)
    this.usableJson = json 
  }
  
  addProperty(){
    this.object.push( { key:"", value:"", keyEditable:true, valueEditable:true, element:"input", inputType:"text", deletable:true, keyPlaceholder:"new key", valuePlaceholder:"new value" } )  
  }
    
  delete(prop){
    const index: number = this.object.indexOf(prop);
    if (index !== -1) {
        this.object.splice(index, 1);
    }
    this.objectedited()
  }
  
  getKeyClass(prop){
      if ( prop.keyClass ){
          return prop.keyClass
      }else if (this.metadata.keysClasses){
          return this.metadata.keysClasses
          }
      }
  
  getTitleClass(){
          return this.metadata.titleClass 
      }
      
  ngOnInit() {
  }

}


