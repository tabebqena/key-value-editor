# 
key-value-editor


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.


## Installation

To install this library, run:

```bash
$ npm install angular-key-value-editor --save
```

and then from your Angular `AppModule`:

```typescript

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// import the editorModule
import { EditorModule } from 'angular-key-value-editor';


import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,    
    EditorModule    // add EditorModule to the import list 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


```

Once imported, you can use its components in your Angular application:

```xml
<obj-editor ></obj-editor>

```

it takes 4 properties:

1- object: the Array of Objects you want to view & edit.
           each object should has at least `key` & `value` properties 
2- metadata : Object containing helpful & general information .
            this is example of complete metadata properties supported:
            `{title:"testEditor", keysEditable: true, valuesEditable: true, propsDeletable:true, extendable:true, extendableTxt :"add Row"}`
3- getUsableArray : `optional` property can bind to function to recieve minimified array of the `Object` in the form : `[ {key: "key", value: "value"}, {key: "key", value: "value"}  ]`

4- getUsableJson : `optional` property can bind to function to recieve minimified Json Object of the `Object` in the form : `{key1: value1, key2: value2}`


the following example shows How to implement this properties.


```xml
<!-- You can now use your library component in app.component.html -->
<h1>
  {{title}}
</h1>

<obj-editor 
   [object] = "testObject"
   (getUsableArray) = "usableArrayRecieved($event)"  
   (getUsableJson)   = "usableJsonRecieved($event)"
   [metadata] ="objMetaData"
></obj-editor>

<div>
    {{ usableArray | json }}
</div>

<div>
    {{ usableJson | json }}
</div>

<div>
    {{ testObject | json }}
</div>


```

the app.component.ts may be as follow:

```typescript

import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'app';
  usableArray =[]
  usableJson = {}
  
  usableArrayRecieved(val){
     console.log(val) 
     this.usableArray = val
    }
  
  usableJsonRecieved(val){
      console.log(val)
      this.usableJson = val
      }
  
  
  testObject = [
     {  key:"key1", keyEditable: false, value:"value1", valueEditable: false , element:"input", inputType:"text", deletable: false  },
     {  key:"key Editable", keyEditable: true , value:"value2", valueEditable: false , element:"input", inputType:"text", deletable: false  },
     {  key:"value Editable", keyEditable: false, value:"value3", valueEditable: true  , element:"input", inputType:"text", deletable: true  },
     {  key:"number editable", keyEditable: false, value:"4"    , valueEditable: true , element:"input", inputType:"number", deletable: false  },
     {  key:" deletable ", keyEditable: false, value:"value5", valueEditable: false , element:"input", inputType:"text", deletable: true  },
     {  key:"minimal ", value:"value6"  },
  ]
  objMetaData = {title:"testEditor", keysEditable: true, valuesEditable: true, propsDeletable:true, extendable:true}    
  
}



```


the 


## Development

To generate all `*.js`, `*.d.ts` and `*.metadata.json` files:

```bash
$ npm run build
```

To lint all `*.ts` files:

```bash
$ npm run lint
```

## License

MIT © [Ahmad Yahia](mailto:tabebqena@gmail.com)
